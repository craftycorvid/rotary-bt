
# Rotary BT

A Bluetooth add-on board for Western Electric rotary phones.

## To-do, prioritized

* [x] Obtain Western Electric model 500
* [ ] Gather reference documents
* [ ] Build mic bias circuit
* [ ] Build mic ADC circuit
* [ ] Build speaker DAC circuit
* [ ] Connect components to microprocessor
* [ ] Write logic for decoding rotary dialer, sampling mic input, generating speaker output, detecting handset off-hook, ringing, and placing calls
* [ ] Build ringer circuit
* [ ] Design rotary-bt Pi hat
* [ ] Fabricate
* [ ] Sell