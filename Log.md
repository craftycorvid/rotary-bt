Project Log
===========

2021-01-24
----------

* Followed guide ["Another How to turn your Pi in a Bluetooth Speaker Tutorial"](https://www.raspberrypi.org/forums/viewtopic.php?f=35&t=235519&sid=5514bd84d9e45a10cdbf91b1b708c4c9) to configure pi as a bluetooth A2DP sink
* Was able to pair several devices: iPhone SE 2nd gen, iPad pro 2nd gen, and a 2019 Macbook Pro
* Was able to play audio from the iPhone through headphones via an HDMI monitor
* There is an issue with iPhone listing bluetooth devices when _pulseaudio_ is in a bad state:
  - resolved by restarting with `pulseaudio --kill` and `pulseaudio --start`
  - the start order of the _pulseaudio_ and _bluetooth_ services could be important
  - PulseAudio recommends _not_ running the sound server as a system service

2021-01-30
----------

* Received and installed Hifiberry DAC+ ADC Pro on Pi Zero W
  - The DAC+ ADC Pro does not have an EEPROM; the overlay must be set manually
* Followed guide https://www.hifiberry.com/docs/software/configuring-linux-3-18-x/:
  - Edited /boot/config.txt and commented out `dtparam=audio=on`
  - Edited /boot/config.txt to include device tree overlay for hifiberry: `dtoverlay=hifiberry-dacplusadcpro`
* Set volume to 50% manually until script can be written to control from bluetooth
    ```

    ```
* Connected jumpers on the hifiberry to enable 5v bias voltage; measured as 2.5v
  on each channel
* The HSP ("Headset Profile") is not covered in the guide, but is necessary to
  connect the mic
* Resources:
  - https://askubuntu.com/questions/354383/headphones-microphone-is-not-working
  - https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Bluetooth/#hsphfp
  - `pavucontrol`
  - https://hackaday.io/project/165208-an-old-rotary-phone-as-bluetooth-set/log

2021-02-02
----------

* Installed oFono
* Edited /etc/dbus-1/system.d/ofono.conf per https://hackaday.io/project/165208-an-old-rotary-phone-as-bluetooth-set/log
* Edited /etc/pulse/default.pa to set headset:
  ```
  load-module module-bluetooth-discover autodetect_mtu=yes headset=ofono
  ```
* Got input from headset routed to Zoom, but there was a long delay and the
  level was poor; see earlier note about 2.5v bias voltage

2021-02-03
----------
* ~Audio lag may be due to too hi-fi data piped over bluetooth~
* Resources:
  - [PulseAudio Troubleshooting](https://wiki.archlinux.org/index.php/PulseAudio/Troubleshooting)

2021-04-24
----------
* Received Raspberry Pi Compute Module 4 and I/O carrier board
* Flashed its eMMC with Raspberry Pi OS and started applying configuration
* Discovered the cause of the latency and syncronization issues. Per,
https://hackaday.io/project/165208-an-old-rotary-phone-as-bluetooth-set/log/162491-setting-up-the-bluetooth,
PulseAudio hard codes the wrong packet size, so it needs to be patched and compiled.
* Compiled Pulseaudio
  - Enabled source downloads in /etc/apt/sources.list
  - Updated with `sudo apt update`
  - Downloaded build deps with `sudo apt-get build-dep pulseaudio`
  - Installed meson `sudo apt install meson`
  - Build and install `meson build`, `cd build`, `ninja`, `sudo ninja install`
  - Check version `pulseaudio --version`
* Resources:
  - [CM4 Datasheet](https://datasheets.raspberrypi.org/cm4/cm4-datasheet.pdf)
  - [Building PulseAudio](https://www.linuxfromscratch.org/blfs/view/svn/multimedia/pulseaudio.html)
  - [Auto switching to HSP](https://askubuntu.com/a/1120106)

2021-04-26
----------
* Can play back audio through CM4, but still not connecting as HSP; consider trying:
  - _btmon_ to see wtf is going on
  - learn about SCO
  - Read the notes at https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Bluetooth/#index8h3 about "HSP problem: the bluetooth sink and source are created, but no audio is being transmitted"
* Ran `sudo hcitool cmd 0x3F 0x01C 0x01 0x02 0x00 0x01 0x01` per https://youness.net/raspberry-pi/how-to-connect-bluetooth-headset-or-speaker-to-raspberry-pi-3
  - Got output:
      ```
      < HCI Command: ogf 0x3f, ocf 0x001c, plen 5
        01 02 00 01 01 
      > HCI Event: 0x0e plen 4
        01 1C FC 00
      ```
